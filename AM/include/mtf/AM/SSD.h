#ifndef MTF_SSD_H
#define MTF_SSD_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE


class SSD : public SSDBase{
public:

	typedef AMParams ParamType;

	SSD(const ParamType *ssd_params = nullptr, const int _n_channels = 1);
	double getLikelihood() const override{
		return exp(-likelihood_alpha * sqrt(-f / (static_cast<double>(patch_size))));
	}
};

_MTF_END_NAMESPACE

#endif