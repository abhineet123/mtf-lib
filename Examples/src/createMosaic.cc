//! main header that provides functions for creating trackers
#include "mtf/mtf.h"
#include <mtf/Tools/pipeline.h>
#include<mtf/Utilities/miscUtils.h>
#include<mtf/Utilities/imgUtils.h>
#include<mtf/Utilities/warpUtils.h>

typedef std::unique_ptr<mtf::TrackerBase> Tracker_;

int main(int argc, char * argv[]){

	if(!readParams(argc, argv)){ return EXIT_FAILURE; }

	Input_ input(getInput(pipeline));
	if(!input->initialize()){ return EXIT_FAILURE; };

	if(init_frame_id>0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; ++frame_id){
		input->update();
	}
	if(res_from_size){
		resx = input->getFrame().cols;
		resy = input->getFrame().rows;
	}
	Tracker_ tracker(mtf::getTracker(mtf_sm, mtf_am, mtf_ssm, mtf_ilm));
	if(!tracker){ return EXIT_FAILURE; }

	resx = input->getFrame().cols;
	resy = input->getFrame().rows;
	cv::Mat init_corners = mtf::utils::getFrameCorners(input->getFrame(), mos_track_border);

	//! dummy SSM to convert the tracker output to a location in the mosaic image
	sim_normalized_init=aff_normalized_init=hom_normalized_init = 0;
	mtf::SSM mos_ssm(mtf::getSSM(mtf_ssm));
	mos_ssm->initialize(init_corners);

	int min_x = mos_border, min_y = mos_border;
	int max_x = mos_border + input->getFrame().cols, max_y = mos_border + input->getFrame().rows;
	//! center of the mosaic image surrounded by the border
	mtf::CornersT init_location;
	init_location <<
		min_x, max_x, max_x, min_x,
		min_y, min_y, max_y, max_y;
	mos_ssm->setCorners(init_location);

	//! dummy AM - 3 channel SSD - to extract pixel values from the current image
	mtf::AM mos_am(mtf::getAM("ssd3", "0"));
	NoProcessing mos_pre_proc(mos_am->inputType());
	mos_pre_proc.initialize(input->getFrame());
	mos_am->setCurrImg(mos_pre_proc.getFrame());

	cv::Mat curr_patch(mos_am->getResY(), mos_am->getResX(), 
		mos_am->getNChannels() == 1 ? CV_8UC1 : CV_8UC3);
	
	GaussianSmoothing pre_proc(tracker->inputType());
	pre_proc.initialize(input->getFrame());
	tracker->setImage(pre_proc.getFrame());

	mtf::PtsT init_pts = mtf::utils::getFramePts(input->getFrame(), 0);
	cv::Mat init_corners_cv_=mtf::utils::getFrameCorners(input->getFrame(), 0);
	
	// mtf::CornersT init_corners_eig;
	// for(int i = 0; i < 4; i++){
		// init_corners_eig(0, i) = init_corners_cv_.at<double>(0, i);
		// init_corners_eig(1, i) = init_corners_cv_.at<double>(1, i);
	// }
	// init_pts.resize(Eigen::NoChange, input->getFrame().cols*input->getFrame().rows);
	// mtf::utils::getGridPtsFromCorners(init_pts, init_corners_eig,
		// input->getFrame().cols, input->getFrame().rows);

	tracker->initialize(init_corners);

	int mosaic_width = input->getFrame().cols + 2 * mos_border;
	int mosaic_height = input->getFrame().rows + 2 * mos_border;

	cv::Mat mosaic_img(mosaic_height, mosaic_width,
		mos_am->getNChannels() == 1 ? CV_8UC1 : CV_8UC3, CV_RGB(0, 0, 0));
	mtf::utils::writePixelsToImage(mosaic_img, mos_am->getPatch(init_pts),
		mos_ssm->getPts(), mos_am->getNChannels());


	double resize_factor_x = static_cast<double>(mos_disp_width) / static_cast<double>(mosaic_img.cols);
	double resize_factor_y = static_cast<double>(mos_disp_height) / static_cast<double>(mosaic_img.rows);
	double mos_resize_factor = resize_factor_x > resize_factor_y ? resize_factor_x : resize_factor_y;
	cv::Mat mosaic_disp_img(mosaic_img.rows*mos_resize_factor, mosaic_img.cols*mos_resize_factor,
		mos_am->getNChannels() == 1 ? CV_8UC1 : CV_8UC3, CV_RGB(0, 0, 0));
	cv::Mat mosaic_disp_corners(2, 4, CV_64FC1);

	printf("Using displayed image of size: %d x %d\n", mosaic_disp_img.cols, mosaic_disp_img.rows);


	cv::Mat prev_corners = tracker->getRegion().clone();
	mtf::CornersT prev_location = mos_ssm->getCorners();
	
	
	mtf::CornersT curr_location;
	while(input->update()){

		pre_proc.update(input->getFrame());
		tracker->update();

		VectorXd curr_warp(mos_ssm->getStateSize());
		VectorXd inv_warp(mos_ssm->getStateSize());

		mos_ssm->estimateWarpFromCorners(curr_warp, init_corners, tracker->getRegion());
		mos_ssm->invertState(inv_warp, curr_warp);
		mos_ssm->applyWarpToCorners(curr_location, prev_location, inv_warp);
		mos_ssm->setCorners(curr_location);

		//mos_ssm->compositionalUpdate(inv_warp);


		tracker->initialize(init_corners);

		//! extract current patch
		mos_pre_proc.update(input->getFrame());
		mtf::PixValT eig_patch = mos_am->getPatch(init_pts);
		//! write the patch to the image at the warped locations given by the tracker
		mtf::utils::writePixelsToImage(mosaic_img, eig_patch, mos_ssm->getPts(), mos_am->getNChannels());

		//! resize the mosaic image to display on screen
		cv::resize(mosaic_img, mosaic_disp_img, mosaic_disp_img.size());
		//! draw the location of the current warped bounding box given by the tracker
		mos_ssm->getCorners(mosaic_disp_corners);
		mosaic_disp_corners *= mos_resize_factor;
		mtf::utils::drawRegion(mosaic_disp_img, mosaic_disp_corners, CV_RGB(0, 255, 0), 2);		
		if(mos_show_grid){
			mtf::PtsT mosaic_disp_grid = mos_ssm->getPts()*mos_resize_factor;
			mtf::utils::drawGrid(mosaic_disp_img, mosaic_disp_grid, mos_ssm->getResX(), mos_ssm->getResY());
		}
		cv::imshow("Mosaic", mosaic_disp_img);

		mtf::utils::drawRegion(input->getFrame(MUTABLE), tracker->getRegion(), CV_RGB(255, 0, 0), 2);
		cv::imshow("Current Image", input->getFrame());

		//! reshape and convert the patch to displayable OpenCV format and show it
		cv::Mat(mos_am->getResY(), mos_am->getResX(), mos_am->getNChannels() == 1 ? CV_64FC1 : CV_64FC3,
			eig_patch.data()).convertTo(curr_patch, curr_patch.type());
		cv::imshow("Current Patch", curr_patch);

		prev_location = curr_location;

		if(cv::waitKey(1) == 27){ break; }
	}
	cv::destroyAllWindows();
	return 0;
}

