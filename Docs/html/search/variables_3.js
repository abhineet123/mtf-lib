var searchData=
[
  ['curr_5ffeat_5fdiff',['curr_feat_diff',['../classPCA.html#a1be871ddf989fd13dfb99c7f68236097',1,'PCA']]],
  ['curr_5fimg',['curr_img',['../classImageBase.html#a05bb3e22989361a082501f48286f58ce',1,'ImageBase']]],
  ['curr_5fimg_5fcv',['curr_img_cv',['../classImageBase.html#a79c1a5f750185761ce3467f9eac640fe',1,'ImageBase']]],
  ['curr_5fjoint_5fhist',['curr_joint_hist',['../classLSCV.html#a5d4c51f8b3ec9caf8ebeb9836d367237',1,'LSCV::curr_joint_hist()'],['../classRSCV.html#a844f72eff0da14c92ab0768f6b44d078',1,'RSCV::curr_joint_hist()'],['../classSCV.html#ab71ff5f212973b5b7d03979f7075fea9',1,'SCV::curr_joint_hist()']]],
  ['curr_5fpix_5fmean',['curr_pix_mean',['../classFMaps.html#adc41982dd716cd062e02074473944196',1,'FMaps::curr_pix_mean()'],['../classSSIM.html#a642d8170cbb2ea7a376cb7431e735064',1,'SSIM::curr_pix_mean()']]]
];
