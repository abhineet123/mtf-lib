#ifndef MTF_FALK_PARAMS_H
#define MTF_FALK_PARAMS_H

#include "SearchMethod.h"

#define FALK_MAX_ITERS 10
#define FALK_EPSILON 0.01
#define FALK_HESS_TYPE 0
#define FALK_SECOND_ORDER_HESS false
#define FALK_SHOW_GRID false
#define FALK_SHOW_PATCH false
#define FALK_PATCH_RESIZE_FACTOR 1.0
#define FALK_WRITE_FRAMES false
#define FALK_UPD_TEMPL false
#define FALK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct FALKParams{
	enum class HessType{ InitialSelf, CurrentSelf, Std };

	int max_iters; //! maximum iterations of the FALK algorithm to run for each frame
	double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations
	HessType hess_type;
	bool sec_ord_hess;
	bool show_grid;
	bool show_patch;
	double patch_resize_factor;
	bool write_frames;
	bool upd_templ;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	FALKParams(int _max_iters, double _epsilon,
		HessType _hess_type, bool _sec_ord_hess,
		bool _show_grid, bool _show_patch,
		double _patch_resize_factor,
		bool _write_frames, bool _upd_templ,
		bool _debug_mode);
	FALKParams(const FALKParams *params = nullptr);
};

_MTF_END_NAMESPACE

#endif

